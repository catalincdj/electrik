const WebSocketServer = require('websocket').server;
const http = require('http');

const server = http.createServer(function(request, response) {
    // process HTTP request. Since we're writing just WebSockets
    // server we don't have to implement anything.
});
server.listen(1337, function() { });

// create the server
wsServer = new WebSocketServer({
    httpServer: server
});

const clients = [];

// WebSocket server
wsServer.on('request', function(request) {
    const connection = request.accept(null, request.origin);

    const id = clients.push(connection);

    // This is the most important callback for us, we'll handle
    // all messages from users here.
    connection.on('message', function(message) {
        if (message.type === 'utf8') {
            const data = JSON.parse(message.utf8Data);

            if (data.type === 'id') {
                connection.token = data.token;
                console.log(Object.keys(clients));
            }
        }
    });

    connection.on('close', function(connection) {
        clients.splice(id, 1);
        console.log(Object.keys(clients));
    });
});