# ElecTrik

Mash-up Web menit sa gestioneze in timp-real a fluxurile de activitati dintr-un lant de furnizori de energie, baterii si acumulatori speciali pentru drone, biciclete si autoturisme electrice.

# Enunt proiect

Sa se creeze un mash-up Web menita sa gestioneze in timp-real a fluxurile de activitati dintr-un lant de furnizori de energie, baterii si acumulatori speciali pentru drone, biciclete si autoturisme electrice.
Minimal, se vor oferi: posibilitatea de a monitoriza de la distanta stocul de baterii, de a rezolva plata energiei livrate, de a notifica anumiti clienti fideli asupra schimbarilor survenite -- de exemplu, modificari de pret, indisponibilitatea unui tip de baterie/acumulator pentru o anumita marca, oferte privind accesorii speciale etc.
Noutatile vizand ofertele vor fi redate via notificari in navigatorul Web si pe baza de fluxuri RSS. Localizarea acestor statii de furnizare de energie electrica va putea fi semnalata pe harta folosind un servicii Web cartografice. De asemenea, se vor oferi rapoarte si vizualizari atractive privitoare la stocurile de baterii/acumulatori, inclusiv situatia vanzarilor per sortiment, perioada de timp sau in functie de un anumit client.
Sistemul va avea o arhitectura modulara, functionalitatile suplimentare fiind implementate de extensii.

# Requirements

- MySQL community server 5.6 or 5.7
- PHP >= 5.6
- nodejs v10.16 and npm v6.13

# Run

cd api
npm install
npm start
cd ../
php -S localhost:80

Open http://localhost in web browser
