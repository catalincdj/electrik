function ServicesModule() {
    const apiService = ApiService();

    const splashScreenService = new SplashScreenService();

    const token = localStorage.getItem('token');

    if (token) {
        if (token.length === 24) { // is user
            window.location.href = './app.html';
        } else if (token.length === 32) { // is supplier
            fillServicesTable();
        } else {
            window.location.href = '/';
        }
    } else {
        window.location.href = '/';
    }

    if (pageState.logoutButtonElement) {
        pageState.logoutButtonElement.addEventListener('click', function () {
            apiService.logout();
        });
    }

    if (pageState.logoHeaderElement) {
        pageState.logoHeaderElement.addEventListener('click', function () {
            const token = localStorage.getItem('token');

            if (token && token.length === 32) { // is supplier
                window.location.href = './supplier.html';
            } else {
                window.location.href = './app.html';
            }
        });
    }

    if (pageState.resetButtonElement) {
        pageState.resetButtonElement.addEventListener('click', function () {
            if (pageState.serviceFormElement) {
                pageState.serviceFormElement.querySelector("input[name=id]").value = "";
                pageState.serviceFormElement.querySelector("input[name=name]").value = "";
                pageState.serviceFormElement.querySelector("input[name=price]").value = "";
                pageState.serviceFormElement.querySelector("select[name=isActive]").value = "";
            }
        });
    }

    if (pageState.saveButtonElement) {
        pageState.saveButtonElement.addEventListener('click', function () {
            if (pageState.serviceFormElement) {
                const data = {
                    id: pageState.serviceFormElement.querySelector("input[name=id]").value,
                    name: pageState.serviceFormElement.querySelector("input[name=name]").value,
                    price: pageState.serviceFormElement.querySelector("input[name=price]").value,
                    isActive: (pageState.serviceFormElement.querySelector("select[name=isActive]").value === 'true')
                };

                apiService.saveService(data).then((response) => {
                    if (response.success) {
                        fillServicesTable();
                    } else {
                        console.log(response.message);
                    }
                });
            }
        });
    }

    if (pageState.serviceTableElement) {
        pageState.serviceTableElement.addEventListener('click', function(event) {
            if (event.target.closest('tr') && !event.target.classList.contains('delete')) {
                const tr = event.target.closest('tr');

                const idItem = pageState.serviceFormElement.querySelector('input[name=id]');
                const nameItem = pageState.serviceFormElement.querySelector('input[name=name]');
                const priceItem = pageState.serviceFormElement.querySelector('input[name=price]');
                const isActive = pageState.serviceFormElement.querySelector('select[name=isActive]');

                if (
                    idItem &&
                    nameItem &&
                    priceItem &&
                    isActive
                ) {
                    idItem.value = tr.querySelector('td[data-id]') ? tr.querySelector('td[data-id]').dataset.id : '';
                    nameItem.value = tr.querySelector('td[data-name]') ? tr.querySelector('td[data-name]').dataset.name : '';
                    priceItem.value = tr.querySelector('td[data-price]') ? tr.querySelector('td[data-price]').dataset.price : '';
                    isActive.value = tr.querySelector('td[data-is-active]').dataset.isActive;
                }
            }
        });
    }

    function fillServicesTable() {
        splashScreenService.show();
        apiService.getServices().then((response) => {
            splashScreenService.hide();
            if (response.success) {
                services = response.data.services;
                let tableRef = document.getElementById('servicesTable').getElementsByTagName('tbody')[0];
                tableRef.innerHTML = "";

                for (let service of services) {
                    let newRow = tableRef.insertRow();

                    let newCell = newRow.insertCell(0);
                    newCell.dataset['id'] = service.id;
                    newCell.innerText = service["id"];

                    newCell = newRow.insertCell(1);
                    newCell.dataset['name'] = service.name;
                    newCell.innerText = service["name"];

                    newCell = newRow.insertCell(2);
                    newCell.dataset['price'] = service.price;
                    newCell.innerText = service["price"];

                    newCell = newRow.insertCell(3);
                    newCell.dataset['isActive'] = service.isActive;

                    if (service["isActive"] === true)
                        newCell.innerText = "Active";
                    else
                        newCell.innerText = "Inactive";

                    newCell = newRow.insertCell(4);
                    newCell.innerHTML = "<button class='delete' value=" + service["id"] + ">Delete</button>";
                }

                addEventToClass("delete", function (event) {
                    apiService.deleteService(event.target.value).then((response) => {
                        if (response.success) {
                            fillServicesTable();
                        } else {
                            console.log(response.message);
                        }
                    });
                });

            } else {
                console.log(response.message);
            }
        });
    }

    function addEventToClass(className, toDo) {
        let classname = document.getElementsByClassName(className);

        for (var i = 0; i < classname.length; i++) {
            classname[i].addEventListener('click', toDo, false);
        }
    }
}

document.addEventListener('DOMContentLoaded', () => {
    ServicesModule();
});