function IndexModule() {
    /* Location Modal Functions */
    if (pageState.locationInput) {
        pageState.locationInput.addEventListener('click', () => {
            if (pageState.locationElement) {
                openLocationModal();

                pageState.locationElement.querySelector('.close-icon').addEventListener('click', (event) => {
                    dismissLocationModal();
                });
            }
        });
    }

    if (pageState.locationSelectButton) {
        pageState.locationSelectButton.addEventListener('click', () => {
            closeLocationModal();
        });
    }

    MapService((map) => {
        let marker = null;

        map.addListener('click', function (event) {
            if (marker) {
                marker.setMap(null);
                marker = null;
            }

            marker = new google.maps.Marker({
                position: {
                    lat: event.latLng.lat(),
                    lng: event.latLng.lng(),
                },
                map: map,
                draggable: true
            });

            marker.addListener('mouseup', onChangeLocation);

            onChangeLocation(event)
        });
    });

    function onChangeLocation(event) {
        if (pageState.locationInput) {
            pageState.locationInput.value = JSON.stringify({
                lat: event.latLng.lat(),
                lng: event.latLng.lng()
            })
        }
    }

    function openLocationModal() {
        if (pageState.locationInput) {
            pageState.locationElement.classList.add('active');
        }

        if (pageState.locationSelectError) {
            pageState.locationSelectError.innerText = '';
            pageState.locationSelectError.classList.add('display-none');
        }
    }

    function dismissLocationModal(mapService) {
        if (pageState.locationElement) {
            pageState.locationElement.classList.remove('active');
        }

        mapService.onChangeLocation = null;
    }

    function closeLocationModal() {
        if (pageState.locationInput.value) {
            if (pageState.locationElement) {
                pageState.locationElement.classList.remove('active');
            }
        } else {
            if (pageState.locationSelectError) {
                pageState.locationSelectError.classList.remove('display-none');
                pageState.locationSelectError.innerText = 'Please select your location';
            }
        }
    }

    /* Submit Form Functions */
    const formService = new FormService();

    function validateForm(form) {
        const values = formService.extractFormValues(form);
        const errors = formService.validateFormValues(values);

        if (errors.length) {
            throw new Error(errors[0].value)
        }
    }

    function getFormValues(form) {
        const values = formService.extractFormValues(form);

        const formValues = {};

        for (const index in values) {
            if (values.hasOwnProperty(index)) {
                formValues[index] = values[index].value;
            }
        }

        return formValues;
    }

    const apiService = new ApiService();

    function registerUser(form) {
        try {
            if (pageState.errorElement) {
                pageState.errorElement.innerText = '';
            }

            validateForm(form);
            const formValues = getFormValues(form);

            apiService.registerUser(formValues).then((response) => {
                if (response.success) {
                    window.location.replace('/app.html');
                } else {
                    pageState.errorElement.innerText = response.message;
                }
            });
        } catch (e) {
            pageState.errorElement.innerText = e.message || e;
        }
    }

    function loginUserOrSupplier(form) {
        try {
            if (pageState.errorElement) {
                pageState.errorElement.innerText = '';
            }

            validateForm(form);
            const formValues = getFormValues(form);

            apiService.loginUserOrSupplier(formValues).then((response) => {
                if (response.success) {
                    window.location.replace('/app.html');
                } else {
                    pageState.errorElement.innerText = response.message;
                }
            });
        } catch (e) {
            pageState.errorElement.innerText = e.message || e;
        }
    }

    function registerSupplier(form) {
        try {
            if (pageState.errorElement) {
                pageState.errorElement.innerText = '';
            }

            validateForm(form);
            const formValues = getFormValues(form);

            apiService.registerSupplier(formValues).then((response) => {
                if (response.success) {
                    window.location.replace('/app.html');
                } else {
                    pageState.errorElement.innerText = response.message;
                }
            });
        } catch (e) {
            pageState.errorElement.innerText = e.message || e;
        }
    }

    /* Page Functions */
    function switchTab(newTabButton, newTab) {
        // reset error
        if (pageState.errorElement) {
            pageState.errorElement.innerText = '';
        }

        // switch tab
        const activeTabButtons = document.querySelectorAll('.tab-button.active');
        if (activeTabButtons && activeTabButtons.length) {
            for (const tabButton of activeTabButtons) {
                tabButton.classList.remove('active');
            }
        }
        newTabButton.classList.add('active');
        const activeTabs = document.querySelectorAll('.tab.active');
        for (const tab of activeTabs) {
            tab.classList.remove('active');
        }
        newTab.classList.add('active');
    }

    if (pageState.tabButtons) {
        for (const tabButton of pageState.tabButtons) {
            tabButton.addEventListener('click', (event) => {
                const newTabButton = event.target;
                const newTabSelector = '.tab[data-tab="' + event.target.dataset.tab + '"]';
                const newTab = document.querySelector(newTabSelector);

                if (newTabButton && newTab) {
                    switchTab(newTabButton, newTab);
                }
            });
        }
    }

    if (pageState.actionButtons) {
        for (const actionButton of pageState.actionButtons) {
            actionButton.addEventListener('click', (event) => {
                event.stopPropagation();
                event.preventDefault();

                const form = event.target.closest('form');

                switch (form.dataset.type) {
                    case 'sign-up-user':
                        registerUser(form);
                        break;
                    case 'sign-in':
                        loginUserOrSupplier(form);
                        break;
                    case 'sign-up-supplier':
                        registerSupplier(form);
                        break;
                }
            });
        }
    }
}

document.addEventListener('DOMContentLoaded', () => {
    IndexModule();
});
