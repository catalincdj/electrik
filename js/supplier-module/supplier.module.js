function SupplierModule() {
    let timeTravel = 0;
    const apiService = ApiService();

    function showAlertAndRedirect(alertClass, message) {
        if (pageState.alertElement) {
            pageState.alertElement.innerText = message;
            pageState.alertElement.classList.add(alertClass);

            setTimeout(() => {
                localStorage.removeItem('token');
                window.location.href = '/';
            }, 7000);
        }
    }

    function getSupplier() {
        apiService.getSupplier().then((response) => {
            if (response.success) {
                function afterMapInit(map) {
                    const supplier = response.data.supplier;

                    const location = JSON.parse(supplier.location);

                    new google.maps.Marker({
                        position: {
                            lat: location.lat,
                            lng: location.lng,
                        },
                        map: map,
                        data: supplier,
                        draggable: false
                    });

                    pageState.sideBarElement.classList.remove('display-none');
                    if (pageState.supplierNameElement) {
                        pageState.supplierNameElement.innerText = supplier.name;
                    }
                }

                MapService(afterMapInit);
            } else {
                showAlertAndRedirect('alert-danger', response.message);
            }
        }, () => {
            window.location.href = '/';
        });
    }

    function openSalesModal() {
        pageState.salesModalElement.classList.remove('display-none');
        timeTravel = 0;

        function getReports() {
            apiService.getAllSalesReport(timeTravel).then((response) => {
                if (response.success) {
                    const image = document.createElement('img');
                    image.src = response.data;
                    image.alt = 'Sales Report';
                    image.onclick = openSalesModal;
                    pageState.salesModalContent.innerHTML = '';
                    pageState.salesModalContent.append(image);
                }
            });
        }

        if (pageState.salesModalContent) {
            getReports();
        }

        if (pageState.closeSalesModalElement) {
            pageState.closeSalesModalElement.onclick = closeSalesModal;
        }

        if (pageState.salesModalArrowLeftElement) {
            pageState.salesModalArrowLeftElement.onclick = () => {
                timeTravel += 7;
                getReports();
            }
        }

        if (pageState.salesModalArrowRightElement) {
            pageState.salesModalArrowRightElement.onclick = () => {
                timeTravel -= 7;
                getReports();
            }
        }
    }

    function closeSalesModal() {
        if (pageState.salesModalElement) {
            pageState.salesModalElement.classList.add('display-none');
        }
    }

    const token = localStorage.getItem('token');

    if (token) {
        if (token.length === 24) { // is user
            window.location.href = '/app.html';
        } else if (token.length === 32) { // is supplier
            getSupplier();
        } else {
            window.location.href = '/';
        }
    } else {
        window.location.href = '/';
    }

    if (pageState.logoutButtonElement) {
        pageState.logoutButtonElement.addEventListener('click', function() {
            apiService.logout();
        });
    }

    if (pageState.logoHeaderElement) {
        pageState.logoHeaderElement.addEventListener('click', function() {
            const token = localStorage.getItem('token');

            if (token && token.length === 32) { // is supplier
                window.location.href = '/supplier.html';
            } else {
                window.location.href = '/app.html';
            }
        });
    }

    if (pageState.salesElement) {
        apiService.getAllSalesReport(timeTravel).then((response) => {
            if (response.success) {
                const image = document.createElement('img');
                image.src = response.data;
                image.alt = 'Sales Report';
                image.onclick = openSalesModal;
                pageState.salesElement.innerHTML = '';
                pageState.salesElement.append(image);
            }
        });
    }
}

document.addEventListener('DOMContentLoaded', () => {
    SupplierModule();
});