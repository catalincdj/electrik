function OffersModule() {
    const apiService = ApiService();

    const splashScreenService = new SplashScreenService();

    const token = localStorage.getItem('token');

    if (token) {
        if (token.length === 24) { // is user
            window.location.href = './app.html';
        } else if (token.length === 32) { // is supplier
            fillOffersTable();
        } else {
            window.location.href = '/';
        }
    } else {
        window.location.href = '/';
    }

    if (pageState.logoutButtonElement) {
        pageState.logoutButtonElement.addEventListener('click', function () {
            apiService.logout();
        });
    }

    if (pageState.logoHeaderElement) {
        pageState.logoHeaderElement.addEventListener('click', function () {
            const token = localStorage.getItem('token');

            if (token && token.length === 32) { // is supplier
                window.location.href = './supplier.html';
            } else {
                window.location.href = './app.html';
            }
        });
    }

    if (pageState.resetButtonElement) {
        pageState.resetButtonElement.addEventListener('click', function () {
            if (pageState.offerFormElement) {
                pageState.offerFormElement.querySelector("input[name=message]").value = "";
                pageState.offerFormElement.querySelector("input[name=assetName]").value = "";
                pageState.offerFormElement.querySelector("select[name=assetType]").value = "";
            }
        });
    }

    if (pageState.saveButtonElement) {
        pageState.saveButtonElement.addEventListener('click', function () {
            if (pageState.offerFormElement) {
                const data = {
                    id: pageState.offerFormElement.querySelector("input[name=id]").value,
                    message: pageState.offerFormElement.querySelector("input[name=message]").value,
                    assetName: pageState.offerFormElement.querySelector("input[name=assetName]").value,
                    assetType: pageState.offerFormElement.querySelector("select[name=assetType]").value
                };

                apiService.saveOffer(data).then((response) => {
                    if (response.success) {
                        fillOffersTable();
                    } else {
                        console.log(response.message);
                    }
                });
            }
        });
    }

    if (pageState.offerTableElement) {
        pageState.offerTableElement.addEventListener('click', function(event) {
            if (event.target.closest('tr') && !event.target.classList.contains('delete')) {
                const tr = event.target.closest('tr');

                const idItem = pageState.offerFormElement.querySelector('input[name=id]');
                const messageItem = pageState.offerFormElement.querySelector('input[name=message]');
                const assetNameItem = pageState.offerFormElement.querySelector('input[name=assetName]');
                const assetTypeItem = pageState.offerFormElement.querySelector('select[name=assetType]');

                if (
                    idItem &&
                    messageItem &&
                    assetNameItem &&
                    assetTypeItem
                ) {
                    idItem.value = tr.querySelector('td[data-id]') ? tr.querySelector('td[data-id]').dataset.id : '';
                    messageItem.value = tr.querySelector('td[data-message]') ? tr.querySelector('td[data-message]').dataset.message : '';
                    assetNameItem.value = tr.querySelector('td[data-asset-name]') ? tr.querySelector('td[data-asset-name]').dataset.assetName : '';
                    assetTypeItem.value = tr.querySelector('td[data-asset-type]') ? tr.querySelector('td[data-asset-type]').dataset.assetType : '';
                }
            }
        });
    }

    function fillOffersTable() {
        splashScreenService.show();
        apiService.getOffers().then((response) => {
            splashScreenService.hide();
            if (response.success) {
                offers = response.data.offers;
                let tableRef = document.getElementById('offersTable').getElementsByTagName('tbody')[0];
                tableRef.innerHTML = "";

                for (let offer of offers) {
                    let newRow = tableRef.insertRow();

                    let newCell = newRow.insertCell(0);
                    newCell.dataset['id'] = offer.id;
                    newCell.innerText = offer["id"];

                    newCell = newRow.insertCell(1);
                    newCell.dataset['message'] = offer.message;
                    newCell.innerText = offer["message"];

                    newCell = newRow.insertCell(2);
                    newCell.dataset['assetName'] = offer.assetName;
                    newCell.innerText = offer["assetName"];

                    newCell = newRow.insertCell(3);
                    newCell.dataset['assetType'] = offer.assetType;

                    if (offer["assetType"] === 1)
                        newCell.innerText = "Product";
                    else
                        newCell.innerText = "Service";

                    newCell = newRow.insertCell(4);
                    newCell.innerHTML = "<button class='delete' value=" + offer["id"] + ">Delete</button>";
                }

                addEventToClass("delete", function (event) {
                    apiService.deleteOffer(event.target.value).then((response) => {
                        if (response.success) {
                            fillOffersTable();
                        } else {
                            console.log(response.message);
                        }
                    });
                });

            } else {
                console.log(response.message);
            }
        });
    }

    function addEventToClass(className, toDo) {
        let classname = document.getElementsByClassName(className);

        for (var i = 0; i < classname.length; i++) {
            classname[i].addEventListener('click', toDo, false);
        }
    }
}

document.addEventListener('DOMContentLoaded', () => {
    OffersModule();
});