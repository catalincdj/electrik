async function ProductsModule() {
    const apiService = ApiService();

    const splashScreenService = new SplashScreenService();

    const token = localStorage.getItem('token');

    if (token) {
        if (token.length === 24) { // is user
            window.location.href = './app.html';
        } else if (token.length === 32) { // is supplier
            fillProductsTable();
        } else {
            window.location.href = '/';
        }
    } else {
        window.location.href = '/';
    }

    if (pageState.logoutButtonElement) {
        pageState.logoutButtonElement.addEventListener('click', function () {
            apiService.logout();
        });
    }

    if (pageState.logoHeaderElement) {
        pageState.logoHeaderElement.addEventListener('click', function () {
            const token = localStorage.getItem('token');

            if (token && token.length === 32) { // is supplier
                window.location.href = './supplier.html';
            } else {
                window.location.href = './app.html';
            }
        });
    }

    if (pageState.resetButtonElement) {
        pageState.resetButtonElement.addEventListener('click', function () {
            if (pageState.productFormElement) {
                pageState.productFormElement.querySelector("input[name=id]").value = "";
                pageState.productFormElement.querySelector("input[name=name]").value = "";
                pageState.productFormElement.querySelector("input[name=qty]").value = "";
                pageState.productFormElement.querySelector("input[name=price]").value = "";
                pageState.productFormElement.querySelector("select[name=inStock]").value = "";
                pageState.productFormElement.querySelector("select[name=type]").value = "";
            }
        });
    }

    if (pageState.saveButtonElement) {
        pageState.saveButtonElement.addEventListener('click', function () {
            if (pageState.productFormElement) {
                const data = {
                    id: pageState.productFormElement.querySelector("input[name=id]").value,
                    name: pageState.productFormElement.querySelector("input[name=name]").value,
                    qty: pageState.productFormElement.querySelector("input[name=qty]").value,
                    price: pageState.productFormElement.querySelector("input[name=price]").value,
                    inStock: (pageState.productFormElement.querySelector("select[name=inStock]").value === 'true'),
                    type: (pageState.productFormElement.querySelector("select[name=type]").value)
                };

                apiService.saveProduct(data).then((response) => {
                    if (response.success) {
                        fillProductsTable();
                    } else {
                        console.log(response.message);
                    }
                });
            }
        });
    }

    if (pageState.productTableElement) {
        pageState.productTableElement.addEventListener('click', function(event) {
            if (event.target.closest('tr') && !event.target.classList.contains('delete')) {
                const tr = event.target.closest('tr');

                const idItem = pageState.productFormElement.querySelector('input[name=id]');
                const nameItem = pageState.productFormElement.querySelector('input[name=name]');
                const qtyItem = pageState.productFormElement.querySelector('input[name=qty]');
                const priceItem = pageState.productFormElement.querySelector('input[name=price]');
                const inStockItem = pageState.productFormElement.querySelector('select[name=inStock]');
                const typeItem = pageState.productFormElement.querySelector('select[name=type]');

                if (
                    idItem &&
                    nameItem &&
                    qtyItem &&
                    priceItem &&
                    inStockItem &&
                    typeItem
                ) {
                    idItem.value = tr.querySelector('td[data-id]') ? tr.querySelector('td[data-id]').dataset.id : '';
                    nameItem.value = tr.querySelector('td[data-name]') ? tr.querySelector('td[data-name]').dataset.name : '';
                    qtyItem.value = tr.querySelector('td[data-qty]') ? tr.querySelector('td[data-qty]').dataset.qty : '';
                    priceItem.value = tr.querySelector('td[data-price]') ? tr.querySelector('td[data-price]').dataset.price : '';
                    inStockItem.value = tr.querySelector('td[data-in-stock]').dataset.inStock;
                    typeItem.value = tr.querySelector('td[data-type]').dataset.type;
                }
            }
        });
    }

    function fillProductsTable() {
        splashScreenService.show();
        apiService.getProducts().then((response) => {
            splashScreenService.hide();
            if (response.success) {
                products = response.data.products;
                let tableRef = document.getElementById('productsTable').getElementsByTagName('tbody')[0];
                tableRef.innerHTML = "";

                for (let product of products) {
                    let newRow = tableRef.insertRow();

                    let newCell = newRow.insertCell(0);
                    newCell.dataset['id'] = product.id;
                    newCell.innerText = product["id"];

                    newCell = newRow.insertCell(1);
                    newCell.dataset['name'] = product.name;
                    newCell.innerText = product["name"];

                    newCell = newRow.insertCell(2);
                    newCell.dataset['qty'] = product.qty;
                    newCell.innerText = product["qty"];

                    newCell = newRow.insertCell(3);
                    newCell.dataset['price'] = product.price;
                    newCell.innerText = product["price"];

                    newCell = newRow.insertCell(4);
                    newCell.dataset['inStock'] = product.inStock;

                    if (product["inStock"] === true)
                        newCell.innerText = "In Stock";
                    else
                        newCell.innerText = "Out Of Stock";

                    newCell = newRow.insertCell(5);
                    newCell.dataset['type'] = product.type;
                    newCell.innerText = product["type"];

                    newCell = newRow.insertCell(6);
                    newCell.innerHTML = "<button class='delete' value=" + product["id"] + ">Delete</button>";
                }

                addEventToClass("delete", function (event) {
                    apiService.deleteProduct(event.target.value).then((response) => {
                        if (response.success) {
                            fillProductsTable();
                        } else {
                            console.log(response.message);
                        }
                    });
                });

            } else {
                console.log(response.message);
            }
        });
    }

    function addEventToClass(className, toDo) {
        let classname = document.getElementsByClassName(className);

        for (var i = 0; i < classname.length; i++) {
            classname[i].addEventListener('click', toDo, false);
        }
    }
}

document.addEventListener('DOMContentLoaded', () => {
    ProductsModule();
});