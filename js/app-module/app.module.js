function AppModule() {
    const apiService = ApiService();

    function showAlert(alertClass, message) {
        if (pageState.alertElement) {
            pageState.alertElement.innerText = message;
            pageState.alertElement.classList.add(alertClass);

            setTimeout(() => {
                pageState.alertElement.innerText = message;
                pageState.alertElement.classList.remove(alertClass);
            }, 7000);
        }
    }

    function showAlertAndRedirect(alertClass, message) {
        if (pageState.alertElement) {
            pageState.alertElement.innerText = message;
            pageState.alertElement.classList.add(alertClass);

            setTimeout(() => {
                localStorage.removeItem('token');
                window.location.href = '/';
            }, 7000);
        }
    }

    function getSuppliers() {
        apiService.getSuppliers().then((response) => {
            if (response.success) {
                function renderList(list, container) {
                    let html = '';

                    if (
                        list.length &&
                        container
                    ) {
                        for (const item of list) {
                            html += '<button class="btn action-primary" data-id="' + item.id + '" data-name="' + item.name + '" data-qty="' + item.qty + '" data-price="' + item.price + '">' + item.name + '</button>';
                        }
                    }

                    container.innerHTML = html;
                }

                function afterMapInit(map) {
                    for (const supplier of response.data.suppliers) {
                        const location = JSON.parse(supplier.location);

                        const marker = new google.maps.Marker({
                            position: {
                                lat: location.lat,
                                lng: location.lng,
                            },
                            map: map,
                            data: supplier,
                            draggable: false
                        });

                        marker.addListener('click', function (event) {
                            const latLng = JSON.stringify(event.latLng);
                            const supplierIndex = response.data.suppliers.findIndex((s) => s.location === latLng);

                            if (supplierIndex > -1) {
                                pageState.sideBarElement.classList.remove('display-none');

                                if (pageState.supplierNameElement) {
                                    pageState.supplierNameElement.innerText = supplier.name;

                                    renderList(supplier.products, pageState.productsWrapperElement);
                                    renderList(supplier.services, pageState.servicesWrapperElement);

                                    const products = pageState.productsWrapperElement.querySelectorAll('button');

                                    for (const product of products) {
                                        product.addEventListener('click', function (event) {
                                            const id = event.target.dataset.id;

                                            if (pageState.shoppingCartElement) {
                                                pageState.shoppingCartElement.classList.remove('display-none');
                                                document.getElementById("out-of-stock").classList.add('display-none');

                                                document.getElementById("description").innerText = event.target.dataset.name;
                                                document.getElementById("price").dataset.initial = event.target.dataset.price;
                                                document.getElementById("price").innerHTML = "$0";
                                                document.getElementById("counter").value = 0;
                                                document.getElementById("counter").dataset.max = event.target.dataset.qty;
                                                document.getElementsByClassName("buy")[0].id = id;
                                                document.getElementsByClassName("buy")[0].dataset.type = "product";
                                            }
                                        });
                                    }

                                    const services = pageState.servicesWrapperElement.querySelectorAll('button');

                                    for (const service of services) {
                                        service.addEventListener('click', function (event) {
                                            const id = event.target.dataset.id;

                                            if (pageState.shoppingCartElement) {
                                                pageState.shoppingCartElement.classList.remove('display-none');
                                                document.getElementById("out-of-stock").classList.add('display-none');

                                                document.getElementById("description").innerText = event.target.dataset.name;
                                                document.getElementById("price").dataset.initial = event.target.dataset.price;
                                                document.getElementById("price").innerHTML = "$0";
                                                document.getElementById("counter").value = 0;
                                                document.getElementById("counter").dataset.max = event.target.dataset.qty;
                                                document.getElementsByClassName("buy")[0].id = id;
                                                document.getElementsByClassName("buy")[0].dataset.type = "service";
                                            }
                                        });
                                    }
                                }
                            } else {
                                showAlert('Unable to find this supplier.');
                            }
                        });
                    }
                }

                MapService(afterMapInit);

                if (pageState.sideBarElement) {
                    pageState.sideBarElement.querySelector('#close-icon').addEventListener('click', function () {
                        pageState.sideBarElement.classList.add('display-none');
                    });

                    if (pageState.shoppingCartElement) {
                        pageState.shoppingCartElement.querySelector('#close-cart').addEventListener('click', function () {
                            pageState.shoppingCartElement.classList.add('display-none');
                        });

                        const priceElement = pageState.shoppingCartElement.querySelector('#price');

                        document.getElementById("plus-btn").addEventListener('click', function (event) {
                            let newValue = parseInt(pageState.counterElement.value) + 1;
                            let maxValue = document.getElementById("counter").dataset.max;

                            if (maxValue !== 0 && newValue > maxValue)
                                document.getElementById("out-of-stock").classList.remove('display-none');
                            else {
                                pageState.counterElement.value = newValue;
                                priceElement.innerText = '$' + (parseFloat(priceElement.dataset.initial) * newValue);
                            }
                        });

                        document.getElementById("minus-btn").addEventListener('click', function (event) {
                            let newValue = parseInt(pageState.counterElement.value) - 1;
                            let maxValue = document.getElementById("counter").dataset.max;

                            if (newValue >= 0) {
                                pageState.counterElement.value = newValue;
                                priceElement.innerText = '$' + (parseFloat(priceElement.dataset.initial) * newValue);
                            }

                            if (newValue === maxValue - 1)
                                document.getElementById("out-of-stock").classList.add('display-none');
                        });

                        document.getElementsByClassName("buy")[0].addEventListener('click', function (e) {
                            const counterValue = parseInt(document.getElementById("counter").value);
                            let datas = document.getElementById(e.target.id).dataset;

                            const data = {
                                id: e.target.id,
                                type: datas.type,
                                qty: counterValue
                            }

                            apiService.buy(data).then((response) => {
                                if (response.success) {
                                    if (datas.type === 'product') {
                                        document.querySelector('[data-id="' + e.target.id + '"]').dataset.qty -= counterValue;
                                    }
                                    pageState.shoppingCartElement.classList.add('display-none');
                                } else {
                                    console.log(response.message);
                                }
                            });
                        });
                    }
                }
            } else {
                showAlertAndRedirect('alert-danger', response.message);
            }
        }, () => {
            window.location.href = '/';
        });
    }

    const token = localStorage.getItem('token');

    if (token) {
        if (token.length === 24) { // is user
            getSuppliers();
        } else if (token.length === 32) { // is supplier
            window.location.href = '/supplier.html';
        } else {
            window.location.href = '/';
        }
    } else {
        window.location.href = '/';
    }

    if (pageState.logoutButtonElement) {
        pageState.logoutButtonElement.addEventListener('click', function () {
            apiService.logout();
        });
    }

    if (pageState.logoHeaderElement) {
        pageState.logoHeaderElement.addEventListener('click', function () {
            const token = localStorage.getItem('token');

            if (token && token.length === 32) { // is supplier
                window.location.href = '/supplier.html';
            } else {
                window.location.href = '/app.html';
            }
        });
    }
}

document.addEventListener('DOMContentLoaded', () => {
    AppModule();
});
