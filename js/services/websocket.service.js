function WebSocketService() {
    window.WebSocket = window.WebSocket || window.MozWebSocket;

    let connection;

    /* constructor */
    (() => {
        connection = new WebSocket('ws://127.0.0.1:1337');
    })();

    function connect(token) {
        connection.onopen = function () {
            connection.send(JSON.stringify({
                type: 'id',
                token
            }));
        };

        connection.onerror = function (error) {
            console.log(error);
        };

        connection.onmessage = function (message) {
            if (API.onMessage) {
                try {
                    const json = JSON.parse(message.data);
                    API.onMessage(json);
                } catch (e) {
                    console.log('This doesn\'t look like a valid JSON: ',
                        message);
                }
            }
        };
    }

    function disconnect() {
        if (connection) {
            connection.close();
        }
    }

    const API = {
        connect,
        disconnect,
        onMessage: null
    };

    return API;
}