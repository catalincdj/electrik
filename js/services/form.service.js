function FormService() {
    const emailRegExp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);

    function extractFormValues(form) {
        const values = {};

        for (const element of form.elements) {
            if (element instanceof HTMLInputElement) {
                values[element.name] = {
                    value: element.value,
                    control: element
                };
            }
        }

        return values;
    }

    function validateFormValues(values) {
        const errors = [];

        for (const index in values) {
            if (values.hasOwnProperty(index)) {
                if ( // check if input field is not empty
                    values[index].control.required &&
                    !values[index].value
                ) {
                    errors.push({
                        value: 'The ' + values[index].control.name + ' field is required',
                        control: values[index].control
                    });
                } else if ( // test if email type input contains a valid email address
                    values[index].control.type === 'email' &&
                    !emailRegExp.test(values[index].value)
                ) {
                    errors.push({
                        value: '"' + values[index].control.value + '" is not a valid email address',
                        control: values[index].control
                    });
                }
            }
        }

        return errors;
    }

    const API = {
        extractFormValues,
        validateFormValues
    };

    return API;
}
