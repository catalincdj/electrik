function SplashScreenService() {
    function show() {
        if (pageState.splashScreen) {
            pageState.splashScreen.classList.add('show');
        }
    }

    async function hide() {
        return new Promise((resolve) => {
            if (pageState.splashScreen) {
                pageState.splashScreen.classList.add('hiding');

                setTimeout(() => {
                    pageState.splashScreen.classList.remove('hiding');
                    pageState.splashScreen.classList.remove('show');
                    resolve();
                }, 700);
            } else {
                resolve();
            }
        });
    }

    const API = {
        show,
        hide
    };

    return API;
}
