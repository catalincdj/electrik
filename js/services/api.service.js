function ApiService() {
    let apiUrl = 'http://localhost:3000';

    async function makeRequest(method, path, data = {}) {


        return new Promise((resolve) => {
            const xhr = new XMLHttpRequest();

            xhr.onreadystatechange = async function () {
                if (xhr.readyState === 4) {
                    if (xhr.response) {
                        const response = JSON.parse(xhr.response);

                        resolve(response);
                    }
                }
            };

            xhr.onerror = async function () {
                resolve({
                    success: false,
                    message: 'Request failed.'
                });
            };

            xhr.open(method, apiUrl + path, true);
            xhr.setRequestHeader('Accept', 'application/json');
            xhr.setRequestHeader('Content-type', 'application/json');
            xhr.send(JSON.stringify(data));
        });
    }

    function registerUser(data) {
        return makeRequest('POST', '/user/sign-up', data)
            .then((response) => {
                if (response.success) {
                    localStorage.setItem('token', response.data.token);
                }

                return response;
            });
    }

    function loginUserOrSupplier(data) {
        return makeRequest('POST', '/user/sign-in', data)
            .then((response) => {
                if (response.success) {
                    localStorage.setItem('token', response.data.token);
                }

                return response;
            });
    }

    function registerSupplier(data) {
        return makeRequest('POST', '/user/supplier-sign-up', data)
            .then((response) => {
                if (response.success) {
                    localStorage.setItem('token', response.data.token);
                }

                return response;
            });
    }

    function getSuppliers() {
        const token = localStorage.getItem('token');
        return makeRequest('GET', '/user/suppliers/' + token);
    }

    function getSupplier() {
        const token = localStorage.getItem('token');
        return makeRequest('GET', '/supplier/' + token);
    }

    function logout() {
        localStorage.removeItem('token');
        window.location.href = '/';
    }

    function saveProduct(data) {
        const token = localStorage.getItem('token');
        return makeRequest('POST', '/supplier/product/' + token, data);
    }

    function getProducts() {
        const token = localStorage.getItem('token');
        return makeRequest('GET', '/supplier/products/' + token);
    }

    function deleteProduct(id) {
        const token = localStorage.getItem('token');
        return makeRequest('DELETE', '/supplier/product/' + id + '/' + token);
    }

    function saveService(data) {
        const token = localStorage.getItem('token');
        return makeRequest('POST', '/supplier/service/' + token, data);
    }

    function getServices() {
        const token = localStorage.getItem('token');
        return makeRequest('GET', '/supplier/services/' + token);
    }

    function deleteService(id) {
        const token = localStorage.getItem('token');
        return makeRequest('DELETE', '/supplier/service/' + id + '/' + token);
    }

    function saveOffer(data) {
        const token = localStorage.getItem('token');
        return makeRequest('POST', '/supplier/offer/' + token, data);
    }

    function getOffers() {
        const token = localStorage.getItem('token');
        return makeRequest('GET', '/supplier/offers/' + token);
    }

    function deleteOffer(id) {
        const token = localStorage.getItem('token');
        return makeRequest('DELETE', '/supplier/offer/' + id + '/' + token);
    }

    function buy(data) {
        const token = localStorage.getItem('token');
        return makeRequest('POST', '/user/buy/' + token, data);
    }

    function setDevoted(data) {
        const token = localStorage.getItem('token');
        return makeRequest('POST', '/supplier/devoted/' + token, data);
    }

    function getClients(data) {
        const token = localStorage.getItem('token');
        return makeRequest('GET', '/supplier/clients/' + token);
    }

    function getAllSalesReport(timeTravel) {
        const token = localStorage.getItem('token');
        return makeRequest('GET', '/supplier/report/sales/' + timeTravel + '/' + token);
    }

    function getClientSalesReport(clientId, timeTravel) {
        const token = localStorage.getItem('token');
        return makeRequest('GET', '/supplier/report/client/' + clientId + '/' + timeTravel + '/' + token);
    }

    const API = {
        registerUser,
        loginUserOrSupplier,
        registerSupplier,
        getSuppliers,
        getSupplier,
        logout,
        saveProduct,
        getProducts,
        deleteProduct,
        saveService,
        getServices,
        deleteService,
        saveOffer,
        getOffers,
        deleteOffer,
        buy,
        setDevoted,
        getClients,
        getAllSalesReport,
        getClientSalesReport
    };

    return API;
}
