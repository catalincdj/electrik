function ClientsModule() {
    let timeTravel = 0;

    const apiService = ApiService();

    const splashScreenService = new SplashScreenService();

    const token = localStorage.getItem('token');

    if (token) {
        if (token.length === 24) { // is user
            window.location.href = './app.html';
        } else if (token.length === 32) { // is supplier
            fillClientsTable();
        } else {
            window.location.href = '/';
        }
    } else {
        window.location.href = '/';
    }

    if (pageState.logoutButtonElement) {
        pageState.logoutButtonElement.addEventListener('click', function () {
            apiService.logout();
        });
    }

    if (pageState.logoHeaderElement) {
        pageState.logoHeaderElement.addEventListener('click', function () {
            const token = localStorage.getItem('token');

            if (token && token.length === 32) { // is supplier
                window.location.href = './supplier.html';
            } else {
                window.location.href = './app.html';
            }
        });
    }

    if (pageState.logoHeaderElement) {
        pageState.logoHeaderElement.addEventListener('click', function () {
            const token = localStorage.getItem('token');

            if (token && token.length === 32) { // is supplier
                window.location.href = './supplier.html';
            } else {
                window.location.href = './app.html';
            }
        });
    }

    function closeSalesModal() {
        if (pageState.salesModalElement) {
            pageState.salesModalElement.classList.add('display-none');
        }
    }

    function fillClientsTable() {
        splashScreenService.show();
        apiService.getClients().then((response) => {
            splashScreenService.hide();
            if (response.success) {
                clients = response.data.clients;
                let tableRef = document.getElementById('clientsTable').getElementsByTagName('tbody')[0];
                tableRef.innerHTML = "";

                for (let client of clients) {
                    let newRow = tableRef.insertRow();

                    let newCell = newRow.insertCell(0);
                    newCell.innerText = client["id"];

                    newCell = newRow.insertCell(1);
                    newCell.innerText = client["name"];

                    newCell = newRow.insertCell(2);
                    newCell.innerText = client["email"];

                    newCell = newRow.insertCell(3);

                    if (client["isDevoted"] === 1)
                        newCell.innerText = "True";
                    else
                        newCell.innerText = "False";

                    newCell = newRow.insertCell(4);
                    newCell.innerHTML = "<button class='secondary chart' value=" + client["id"] + ">View Sales</button>" +
                        "<button class='devoted' value=" + client["id"] + ";" + client["isDevoted"] + ">Change devoted</button>";
                }

                addEventToClass("devoted", function (event) {
                    let values = event.target.value.split(";");

                    if (values[1] === "1")
                        values[1] = "0"
                    else
                        values[1] = "1"

                    const data = {
                        id: values[0],
                        isDevoted: values[1]
                    };

                    apiService.setDevoted(data).then((response) => {

                        if (response.success) {
                            fillClientsTable();
                        } else {
                            console.log(response.message);
                        }

                    })
                });

                addEventToClass("chart", function (event) {
                    pageState.salesModalElement.classList.remove('display-none');
                    timeTravel = 0;
                    let clientId = event.target.value;

                    function getReports() {
                        apiService.getClientSalesReport(clientId, timeTravel).then((response) => {
                            if (response.success) {


                                const image = document.createElement('img');
                                image.src = response.data;
                                image.alt = 'Sales Report';
                                pageState.salesModalContent.innerHTML = '';
                                pageState.salesModalContent.append(image);
                            } else {
                                console.log(response.message);
                            }
                        });
                    }

                    debugger;

                    if (pageState.salesModalContent) {
                        getReports();
                    }

                    if (pageState.closeSalesModalElement) {
                        pageState.closeSalesModalElement.onclick = closeSalesModal;
                    }

                    if (pageState.salesModalArrowLeftElement) {
                        pageState.salesModalArrowLeftElement.onclick = () => {
                            timeTravel += 7;
                            getReports();
                        }
                    }

                    if (pageState.salesModalArrowRightElement) {
                        pageState.salesModalArrowRightElement.onclick = () => {
                            timeTravel -= 7;
                            getReports();
                        }
                    }
                });

                addEventToClass("delete", function () {

                });
            } else {
                console.log(response.message);
            }
        });
    }

    function addEventToClass(className, toDo) {
        let classname = document.getElementsByClassName(className);

        for (var i = 0; i < classname.length; i++) {
            classname[i].addEventListener('click', toDo, false);
        }
    }
}

document.addEventListener('DOMContentLoaded', () => {
    ClientsModule();
});