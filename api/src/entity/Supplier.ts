import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity({
    name: 'suppliers'
})
export class Supplier {
    public static authTokenLength = 32;

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    email: string;

    @Column()
    password: string;

    @Column()
    location: string;

    @Column()
    token: string;

    @Column()
    tokenExpireAt: Date;
}
