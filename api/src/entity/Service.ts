import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity({
    name: 'services'
})
export class Service {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    supplierId: number;

    @Column()
    name: string;

    @Column('decimal', { precision: 5, scale: 2 })
    price: number;

    @Column()
    isActive: boolean;
}
