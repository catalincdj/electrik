import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity({
    name: 'supplier_user'
})
export class SupplierUser {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    supplierId: number;

    @Column()
    userId: number;

    @Column()
    isDevoted: boolean;
}
