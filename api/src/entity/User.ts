import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity({
    name: 'users'
})
export class User {
    public static authTokenLength = 24;

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    email: string;

    @Column()
    password: string;

    @Column()
    token: string;

    @Column()
    tokenExpireAt: Date;
}
