import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity({
    name: 'offers'
})
export class Offer {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    supplierId: number;

    @Column()
    message: string;

    @Column()
    assetName: string;

    @Column()
    assetType: number;
}
