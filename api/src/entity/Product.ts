import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity({
    name: 'products'
})
export class Product {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    supplierId: number;

    @Column()
    name: string;

    @Column()
    qty: number;

    @Column('decimal', { precision: 5, scale: 2 })
    price: number;

    @Column()
    inStock: boolean;

    @Column()
    type: string;
}
