import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity({
    name: 'sales'
})
export class Sale {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    userId: number;

    @Column()
    assetId: number;

    @Column()
    type: string;

    @Column()
    qty: number;

    @Column('decimal', { precision: 5, scale: 2 })
    price: number;

    @Column()
    createdAt: Date;
}
