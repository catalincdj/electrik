import {Module} from "@nestjs/common";
import {SupplierController} from "./supplier.controller";
import {TypeOrmModule} from "@nestjs/typeorm";
import {User} from "../entity/User";
import {Supplier} from "../entity/Supplier";
import {Product} from "../entity/Product";
import {Service} from "../entity/Service";
import {Offer} from "../entity/Offer";
import {SupplierUser} from "../entity/SupplierUser";
import {Sale} from "../entity/Sale";
import {NotificationHelper} from "./notification.helper";

@Module({
    imports: [
        TypeOrmModule.forFeature([User, Supplier, Product, Service, Offer, SupplierUser, Sale]),
    ],
    controllers: [
        SupplierController
    ],
    providers: [
        NotificationHelper
    ]
})
export class SupplierModule {
}
