import {Body, Controller, Delete, Get, Param, Post} from "@nestjs/common";
import {Connection, Repository} from "typeorm";
import * as moment from 'moment';
import {InjectRepository} from "@nestjs/typeorm";
import {Supplier} from "../entity/Supplier";
import {Product} from "../entity/Product";
import {Service} from "../entity/Service";
import {Offer} from "../entity/Offer";
import {SupplierUser} from "../entity/SupplierUser";
import {Sale} from "../entity/Sale";
import {CanvasRenderService} from 'chartjs-node-canvas';
import {NotificationHelper} from "./notification.helper";

@Controller('supplier')
export class SupplierController {

    constructor(
        private connection: Connection,
        @InjectRepository(Supplier)
        private supplierRepository: Repository<Supplier>,
        @InjectRepository(Product)
        private productRepository: Repository<Product>,
        @InjectRepository(Service)
        private serviceRepository: Repository<Service>,
        @InjectRepository(Offer)
        private offerRepository: Repository<Offer>,
        @InjectRepository(SupplierUser)
        private supplierUserRepository: Repository<SupplierUser>,
        @InjectRepository(Sale)
        private salesRepository: Repository<Sale>,
        private notificationHelper: NotificationHelper
    ) {
    }

    @Get('/:token')
    public async getSupplier(@Param() params) {
        const token = params.token;

        const result = {
            success: false,
            message: '',
            data: {}
        };

        try {
            const supplier = await this.tryAuthenticate(token);

            result.data['supplier'] = supplier;
            result.data['supplier']['products'] = await this.productRepository.find({
                supplierId: supplier.id
            });
            result.data['supplier']['services'] = await this.serviceRepository.find({
                supplierId: supplier.id
            });
            result.data['supplier']['offers'] = await this.offerRepository.find({
                supplierId: supplier.id
            });
            result.data['supplier']['clients'] = await this.connection.query(
                "SELECT users.id, users.name, users.email, supplier_user.isDevoted FROM users JOIN supplier_user ON users.id = supplier_user.userId WHERE supplier_user.supplierId = ?",
                [supplier.id]
            );

            result.success = true;
        } catch (e) {
            result.message = e.message;
        }

        return result;
    }

    @Get('/products/:token')
    public async getProducts(@Param() params) {
        const token = params.token;

        const result = {
            success: false,
            message: '',
            data: {}
        };

        try {
            const supplier = await this.tryAuthenticate(token);

            result.data['products'] = await this.productRepository.find({
                supplierId: supplier.id
            });
            result.success = true;
        } catch (e) {
            result.message = e.message;
        }

        return result;
    }

    @Post('/product/:token')
    public async postProduct(@Param() params, @Body() body) {
        const token = params.token;

        const result = {
            success: false,
            message: '',
            data: {}
        };

        try {
            const supplier = await this.tryAuthenticate(token);
            this.checkRequired(['name', 'qty', 'price', 'inStock'], body);

            let product;
            if (body['id']) { // update
                product = await this.productRepository.findOne(body['id']);
            } else { // create
                product = new Product();
                product.supplierId = supplier.id;
            }

            if (!product) {
                throw new Error('Cannot create product.');
            }

            if (body.id) {
                if (parseFloat(product.price) !== parseFloat(body.price)) {
                    this.notificationHelper.notifyDevotedClients('Product Price Changed', 'The product "' + product.name + '" has changed price from: ' + product.price + ' to: ' + body['price']);
                }

                if (parseInt(product.qty) !== parseInt(body.qty)) {
                    this.notificationHelper.notifyDevotedClients('Product Quantity Changed', 'The product "' + product.name + '" has changed quantity from: ' + product.qty + ' to: ' + body['qty']);
                }

                if (!!product.inStock !== !!body.inStock) {
                    this.notificationHelper.notifyDevotedClients('Product Stock Status Changed', 'The product "' + product.name + '" has changed stock status from: ' + product.inStock + ' to: ' + body['inStock']);
                }
            }

            product.name = body['name'];
            product.qty = body['qty'];
            product.price = body['price'];
            product.inStock = body['inStock'];
            product.type = body['type'];

            await this.connection.manager.save(product);

            result.success = true;
        } catch (e) {
            result.message = e.message;
        }

        return result;
    }

    @Delete('/product/:id/:token')
    public async deleteProduct(@Param() params) {
        const token = params.token;

        const result = {
            success: false,
            message: '',
            data: {}
        };

        try {
            const supplier = await this.tryAuthenticate(token);

            if (params.id) {
                await this.connection.manager.delete(Product, {
                    id: params.id,
                    supplierId: supplier.id,
                });
            }

            result.success = true;
        } catch (e) {
            result.message = e.message;
        }

        return result;
    }

    @Get('/services/:token')
    public async getServices(@Param() params) {
        const token = params.token;

        const result = {
            success: false,
            message: '',
            data: {}
        };

        try {
            const supplier = await this.tryAuthenticate(token);

            result.data['services'] = await this.serviceRepository.find({
                supplierId: supplier.id
            });
            result.success = true;
        } catch (e) {
            result.message = e.message;
        }

        return result;
    }

    @Post('/service/:token')
    public async postService(@Param() params, @Body() body) {
        const token = params.token;

        const result = {
            success: false,
            message: '',
            data: {}
        };

        try {
            const supplier = await this.tryAuthenticate(token);
            this.checkRequired(['name', 'price', 'isActive'], body);

            let service;
            if (body['id']) { // update
                service = await this.serviceRepository.findOne(body['id']);
            } else { // create
                service = new Service();
                service.supplierId = supplier.id;
            }

            if (!service) {
                throw new Error('Cannot create service.');
            }

            if (body.id) {
                if (parseFloat(service.price) !== parseFloat(body.price)) {
                    this.notificationHelper.notifyDevotedClients('Service Price Changed', 'The service "' + service.name + '" has changed price from: ' + service.price + ' to: ' + body['price']);
                }

                if (!!service.isActive !== !!body.isActive) {
                    this.notificationHelper.notifyDevotedClients('Service Active Status Changed', 'The service "' + service.name + '" has changed activity status from: ' + service.isActive + ' to: ' + body['isActive']);
                }
            }

            service.name = body['name'];
            service.price = body['price'];
            service.isActive = body['isActive'];

            await this.connection.manager.save(service);

            result.success = true;
        } catch (e) {
            result.message = e.message;
        }

        return result;
    }

    @Delete('/service/:id/:token')
    public async deleteService(@Param() params) {
        const token = params.token;

        const result = {
            success: false,
            message: '',
            data: {}
        };

        try {
            const supplier = await this.tryAuthenticate(token);

            if (params.id) {
                await this.connection.manager.delete(Service, {
                    id: params.id,
                    supplierId: supplier.id,
                });
            }

            result.success = true;
        } catch (e) {
            result.message = e.message;
        }

        return result;
    }

    @Get('/offers/:token')
    public async getOffers(@Param() params) {
        const token = params.token;

        const result = {
            success: false,
            message: '',
            data: {}
        };

        try {
            const supplier = await this.tryAuthenticate(token);

            result.data['offers'] = await this.offerRepository.find({
                supplierId: supplier.id
            });
            result.success = true;
        } catch (e) {
            result.message = e.message;
        }

        return result;
    }

    @Post('/offer/:token')
    public async postOffer(@Param() params, @Body() body) {
        const token = params.token;

        const result = {
            success: false,
            message: '',
            data: {}
        };

        try {
            const supplier = await this.tryAuthenticate(token);
            this.checkRequired(['message', 'assetName', 'assetType'], body);

            let offer;
            if (body['id']) { // update
                offer = await this.offerRepository.findOne(body['id']);
            } else { // create
                offer = new Offer();
                offer.supplierId = supplier.id;
            }

            if (!offer) {
                throw new Error('Cannot create offer.');
            }

            if (!body['id']) {
                this.notificationHelper.notifyDevotedClients('New Offer Added', 'New offer "' + body.message + '" was added to Electrik.');
            }

            offer.message = body['message'];
            offer.assetName = body['assetName'];
            offer.assetType = body['assetType'];

            await this.connection.manager.save(offer);

            result.success = true;
        } catch (e) {
            result.message = e.message;
        }

        return result;
    }

    @Delete('/offer/:id/:token')
    public async deleteOffer(@Param() params) {
        const token = params.token;

        const result = {
            success: false,
            message: '',
            data: {}
        };

        try {
            const supplier = await this.tryAuthenticate(token);

            if (params.id) {
                await this.connection.manager.delete(Offer, {
                    id: params.id,
                    supplierId: supplier.id,
                });
            }

            result.success = true;
        } catch (e) {
            result.message = e.message;
        }

        return result;
    }

    @Post('/devoted/:token')
    public async setDevoted(@Param() params, @Body() body) {
        const token = params.token;

        const result = {
            success: false,
            message: '',
            data: {}
        };

        try {
            const supplier = await this.tryAuthenticate(token);
            this.checkRequired(['id', 'isDevoted'], body);

            let supplierUser;
            if (body['id']) {
                supplierUser = await this.supplierUserRepository.findOneOrFail({
                    userId: body['id'],
                    supplierId: supplier.id
                });
            }

            if (!supplierUser) {
                throw new Error('Cannot update client.');
            }

            supplierUser.isDevoted = body['isDevoted'] === '1';

            await this.connection.manager.save(supplierUser);

            result.success = true;
        } catch (e) {
            result.message = e.message;
        }

        return result;
    }

    @Get('/clients/:token')
    public async getClients(@Param() params) {
        const token = params.token;

        const result = {
            success: false,
            message: '',
            data: {}
        };

        try {
            const supplier = await this.tryAuthenticate(token);

            result.data['clients'] = await this.connection.query(
                "SELECT users.id, users.name, users.email, supplier_user.isDevoted FROM users JOIN supplier_user ON users.id = supplier_user.userId WHERE supplier_user.supplierId = ?",
                [supplier.id]
            );

            result.success = true;
        } catch (e) {
            result.message = e.message;
        }

        return result;
    }

    @Get('/report/sales/:timeTravel/:token')
    public async getAllSalesReport(@Param() params) {
        const timeTravel = params.timeTravel;
        const token = params.token;

        const result = {
            success: false,
            message: '',
            data: {}
        };

        try {
            const supplier = await this.tryAuthenticate(token);

            const serviceSales = await this.connection.query(
                "SELECT sales.* FROM supplier_user JOIN sales ON supplier_user.userId = sales.userId WHERE supplier_user.supplierId = ? AND type = 'service' ORDER BY sales.createdAt ASC",
                [supplier.id]
            );

            const batterySales = await this.connection.query(
                "SELECT sales.* FROM supplier_user JOIN sales ON supplier_user.userId = sales.userId JOIN products ON sales.assetId = products.id WHERE supplier_user.supplierId = ? AND sales.type = 'product' AND products.type = 'battery' ORDER BY sales.createdAt ASC",
                [supplier.id]
            );

            const chargerSales = await this.connection.query(
                "SELECT sales.* FROM supplier_user JOIN sales ON supplier_user.userId = sales.userId JOIN products ON sales.assetId = products.id WHERE supplier_user.supplierId = ? AND sales.type = 'product' AND products.type = 'charger' ORDER BY sales.createdAt ASC",
                [supplier.id]
            );

            const salesMeta = {
                type: 'bar',
                data: {
                    labels: [],
                    datasets: [
                        {
                            label: 'Number of Sales (Services)',
                            data: [0, 0, 0, 0, 0, 0, 0],
                            fill: false,
                            backgroundColor: [
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                            ],
                            borderColor: [
                                'rgba(54, 162, 235, 1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(54, 162, 235, 1)',
                            ],
                            borderWidth: 1
                        },
                        {
                            label: 'Number of Sales (Batteries)',
                            data: [0, 0, 0, 0, 0, 0, 0],
                            fill: false,
                            backgroundColor: [
                                'rgba(54, 235, 162, 0.2)',
                                'rgba(54, 235, 162, 0.2)',
                                'rgba(54, 235, 162, 0.2)',
                                'rgba(54, 235, 162, 0.2)',
                                'rgba(54, 235, 162, 0.2)',
                                'rgba(54, 235, 162, 0.2)',
                                'rgba(54, 235, 162, 0.2)',
                            ],
                            borderColor: [
                                'rgba(54, 235, 162, 1)',
                                'rgba(54, 235, 162, 1)',
                                'rgba(54, 235, 162, 1)',
                                'rgba(54, 235, 162, 1)',
                                'rgba(54, 235, 162, 1)',
                                'rgba(54, 235, 162, 1)',
                                'rgba(54, 235, 162, 1)',
                            ],
                            borderWidth: 1
                        },
                        {
                            label: 'Number of Sales (Chargers)',
                            data: [0, 0, 0, 0, 0, 0, 0],
                            fill: false,
                            backgroundColor: [
                                'rgba(235, 54, 162, 0.2)',
                                'rgba(235, 54, 162, 0.2)',
                                'rgba(235, 54, 162, 0.2)',
                                'rgba(235, 54, 162, 0.2)',
                                'rgba(235, 54, 162, 0.2)',
                                'rgba(235, 54, 162, 0.2)',
                                'rgba(235, 54, 162, 0.2)',
                            ],
                            borderColor: [
                                'rgba(235, 54, 162, 1)',
                                'rgba(235, 54, 162, 1)',
                                'rgba(235, 54, 162, 1)',
                                'rgba(235, 54, 162, 1)',
                                'rgba(235, 54, 162, 1)',
                                'rgba(235, 54, 162, 1)',
                                'rgba(235, 54, 162, 1)',
                            ],
                            borderWidth: 1
                        }
                    ]
                },
                options: {
                    scales: {
                        yAxes: [{
                            display: true,
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    },
                }
            };

            const endDate = moment().subtract(parseInt(timeTravel, 10), 'days');

            for (let i = 6; i >= 0; i--) {
                const day = endDate.clone().subtract(i, 'days');
                const date = day.date();
                const month = (day.month() + 1) < 10 ? '0' + (day.month() + 1) : (day.month() + 1);
                const year = day.year();
                const label = date + '/' + month + '/' + year;

                salesMeta.data.labels.push(label);

                for (const sale of serviceSales) {
                    const createdAt = moment(sale.createdAt);

                    if (
                        createdAt.date() === date &&
                        createdAt.month() === day.month() &&
                        createdAt.year() === day.year()
                    ) {
                        salesMeta.data.datasets[0].data[6 - i]++;
                    }
                }

                for (const sale of batterySales) {
                    const createdAt = moment(sale.createdAt);

                    if (
                        createdAt.date() === date &&
                        createdAt.month() === day.month() &&
                        createdAt.year() === day.year()
                    ) {
                        salesMeta.data.datasets[1].data[6 - i]++;
                    }
                }

                for (const sale of chargerSales) {
                    const createdAt = moment(sale.createdAt);

                    if (
                        createdAt.date() === date &&
                        createdAt.month() === day.month() &&
                        createdAt.year() === day.year()
                    ) {
                        salesMeta.data.datasets[2].data[6 - i]++;
                    }
                }
            }

            const canvasRenderService = new CanvasRenderService(300, 300);

            result.data = await canvasRenderService.renderToDataURL(salesMeta);
            result.success = true;
        } catch (e) {
            result.message = e.message;
        }

        return result;
    }

    @Get('/report/client/:id/:timeTravel/:token')
    public async getClientSalesReport(@Param() params) {
        const id = params.id;
        const timeTravel = params.timeTravel;
        const token = params.token;

        const result = {
            success: false,
            message: '',
            data: {}
        };

        try {
            const supplier = await this.tryAuthenticate(token);

            const sales = await this.connection.query(
                "SELECT sales.* FROM supplier_user JOIN sales ON supplier_user.userId = sales.userId WHERE sales.userId = ? AND supplier_user.supplierId = ? ORDER BY sales.createdAt ASC",
                [
                    id,
                    supplier.id
                ]
            );

            const salesMeta = {
                type: 'line',
                data: {
                    labels: [],
                    datasets: [
                        {
                            label: 'Number of Sales',
                            data: [0, 0, 0, 0, 0, 0, 0],
                            fill: false,
                            backgroundColor: [
                                'rgba(54, 162, 235, 0.2)'
                            ],
                            borderColor: [
                                'rgba(54, 162, 235, 1)'
                            ],
                            borderWidth: 1
                        }
                    ]
                },
                options: {
                    scales: {
                        yAxes: [{
                            display: true,
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    },
                }
            };

            const endDate = moment().subtract(parseInt(timeTravel, 10), 'days');

            for (let i = 6; i >= 0; i--) {
                const day = endDate.clone().subtract(i, 'days');
                const date = day.date();
                const month = (day.month() + 1) < 10 ? '0' + (day.month() + 1) : (day.month() + 1);
                const year = day.year();
                const label = date + '/' + month + '/' + year;

                salesMeta.data.labels.push(label);

                for (const sale of sales) {
                    const createdAt = moment(sale.createdAt);

                    if (
                        createdAt.date() === date &&
                        createdAt.month() === day.month() &&
                        createdAt.year() === day.year()
                    ) {
                        salesMeta.data.datasets[0].data[6 - i]++;
                    }
                }
            }

            const canvasRenderService = new CanvasRenderService(600, 600);

            result.data = await canvasRenderService.renderToDataURL(salesMeta);
            result.success = true;
        } catch (e) {
            result.message = e.message;
        }

        return result;
    }

    private async tryAuthenticate(token) {
        if (token.length === Supplier.authTokenLength) {
            let supplier = await this.supplierRepository.findOne({
                token
            });

            if (supplier) {
                if (moment(supplier.tokenExpireAt).diff(moment()) < 0) {
                    throw new Error('Token expired.');
                } else {
                    return supplier;
                }
            } else { // try to log in supplier
                throw new Error('Unable to authenticate.');
            }
        } else {
            throw new Error('Unable to authenticate.');
        }
    }

    private checkRequired(fields, body) {
        for (const field of fields) {
            if (
                body[field] === undefined ||
                body[field] === ''
            ) {
                throw new Error(field + ' field is required.');
            }
        }
    }
}
