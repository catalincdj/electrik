import {Feed} from 'feed';
import {Injectable} from '@nestjs/common';
const fs = require('fs');
const Parser = require('rss-parser');

@Injectable()
export class NotificationHelper {
    private feed;

    constructor() {
        this.feed = new Feed({
            title: 'Electrik Feed',
            description: 'This is Electrik feed!',
            id: 'http://example.com/',
            link: 'http://example.com/',
            language: 'en', // optional, used only in RSS 2.0, possible values: http://www.w3.org/TR/REC-html40/struct/dirlang.html#langcodes
            copyright: 'All rights reserved 2020, Electrik',
            generator: 'main', // optional, default = 'Feed for Node.js'
            author: {
                name: 'Electrik Staff',
                email: 'staff@electrik.com'
            }
        });

        const feedString = fs.readFileSync('/Users/ciobmaxim/Desktop/electrik/api/src/rss.xml');

        setTimeout(async () => {
            const parser = new Parser();
            const rssJson = await parser.parseString(feedString.toString());

            for (const item of rssJson.items) {
                this.feed.addItem({
                    title: item.title,
                    link: item.link,
                    date: new Date(item.isoDate),
                    content: item.content
                });
            }
        }, 1);
    }

    public notifyDevotedClients(title, message) {
        this.addToRssFeed(title, message);
    }

    private addToRssFeed(title, message) {
        this.feed.addItem({
            title,
            link: 'http://www.mywebsite.com/',
            date: new Date(),
            content: message
        });

        this.saveRssToFile();
    }

    private saveRssToFile() {
        const feedString = this.feed.rss2();
        fs.writeFileSync('/Users/ciobmaxim/Desktop/electrik/api/src/rss.xml', feedString);
    }
}
