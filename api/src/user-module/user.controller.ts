import {Body, Controller, Get, Param, Post} from "@nestjs/common";
import {User} from "../entity/User";
import {Connection, Repository} from "typeorm";
import * as crypto from 'crypto';
import * as moment from 'moment';
import {InjectRepository} from "@nestjs/typeorm";
import {Supplier} from "../entity/Supplier";
import {Product} from "../entity/Product";
import {Service} from "../entity/Service";
import {Sale} from "../entity/Sale";
import {SupplierUser} from "../entity/SupplierUser";

@Controller('user')
export class UserController {

    constructor(
        private connection: Connection,
        @InjectRepository(User)
        private userRepository: Repository<User>,
        @InjectRepository(Supplier)
        private supplierRepository: Repository<Supplier>,
        @InjectRepository(Product)
        private productRepository: Repository<Product>,
        @InjectRepository(Service)
        private serviceRepository: Repository<Service>,
        @InjectRepository(SupplierUser)
        private supplierUserRepository: Repository<SupplierUser>
    ) {
    }

    @Post('sign-up')
    public async signUp(@Body() body) {
        const result = {
            success: false,
            message: '',
            data: {}
        };

        try {
            if (
                !body ||
                !body.name ||
                !body.email ||
                !body.password
            ) {
                throw new Error('Bad request');
            }

            const userExists = await this.userRepository.findOne({
                email: body.email
            });

            const supplierExists = await this.supplierRepository.findOne({
                email: body.email
            });

            if (
                userExists ||
                supplierExists
            ) {
                throw new Error('An user with same email address already exists.');
            }

            const passwordHash = crypto.createHash('sha256');

            const user = new User();

            user.name = body.name;
            user.email = body.email;
            user.password = passwordHash.update(body.password).digest('hex');
            user.token = this.generateToken(User.authTokenLength);
            user.tokenExpireAt = moment().add(1, 'day').toDate();

            await this.connection.manager.save(user);

            result.data['token'] = user.token;
            result.success = true;
        } catch (e) {
            result.message = e.message;
        }

        return result;
    }

    @Post('sign-in')
    public async signIn(@Body() body) {
        const result = {
            success: false,
            message: '',
            data: {}
        };

        try {
            if (
                !body ||
                !body.email ||
                !body.password
            ) {
                throw new Error('Bad request');
            }

            const passwordHash = crypto.createHash('sha256').update(body.password).digest('hex');
            const token = this.generateToken(User.authTokenLength);
            const tokenExpireAt = moment().add(1, 'day').toDate();

            let user = await this.userRepository.findOne({
                email: body.email,
                password: passwordHash
            });

            if (user) {
                user.token = token;
                user.tokenExpireAt = tokenExpireAt
            } else { // try to log in supplier
                user = await this.supplierRepository.findOne({
                    email: body.email,
                    password: passwordHash
                });

                if (!user) {
                    throw new Error('User not found. Wrong credentials provided.')
                }

                user.token = this.generateToken(Supplier.authTokenLength);
                user.tokenExpireAt = tokenExpireAt
            }

            await this.connection.manager.save(user);

            result.data['token'] = user.token;
            result.success = true;
        } catch (e) {
            result.message = e.message;
        }

        return result;
    }

    @Post('supplier-sign-up')
    public async supplierSignUp(@Body() body) {
        const result = {
            success: false,
            message: '',
            data: {}
        };

        try {
            if (
                !body ||
                !body.name ||
                !body.email ||
                !body.password ||
                !body.location
            ) {
                throw new Error('Bad request');
            }

            const userExists = await this.userRepository.findOne({
                email: body.email
            });

            const supplierExists = await this.supplierRepository.findOne({
                email: body.email
            });

            if (
                userExists ||
                supplierExists
            ) {
                throw new Error('An user with same email address already exists.');
            }

            const passwordHash = crypto.createHash('sha256');

            const supplier = new Supplier();

            supplier.name = body.name;
            supplier.email = body.email;
            supplier.password = passwordHash.update(body.password).digest('hex');
            supplier.location = body.location;
            supplier.token = this.generateToken(Supplier.authTokenLength);
            supplier.tokenExpireAt = moment().add(1, 'day').toDate();

            await this.connection.manager.save(supplier);

            result.data['token'] = supplier.token;
            result.success = true;
        } catch (e) {
            result.message = e.message;
        }

        return result;
    }

    @Get('suppliers/:token')
    public async getSuppliers(@Param() params) {
        const token = params.token;

        const result = {
            success: false,
            message: '',
            data: {}
        };

        try {
            await this.tryAuthenticate(token);

            const suppliers = await this.supplierRepository.find();

            for (const supplier of suppliers) {
                supplier['products'] = await this.productRepository.find({
                    supplierId: supplier.id,
                    inStock: true
                });

                supplier['services'] = await this.serviceRepository.find({
                    supplierId: supplier.id,
                    isActive: true
                });
            }

            result.data['suppliers'] = suppliers;
            result.success = true;
        } catch (e) {
            result.message = e.message;
        }

        return result;
    }

    @Post('buy/:token')
    public async buy(@Param() params, @Body() body) {
        const token = params.token;

        const result = {
            success: false,
            message: '',
            data: {}
        };

        try {
            const user = await this.tryAuthenticate(token);

            if (
                !body.id ||
                !body.qty ||
                !body.type ||
                (body.type !== 'product' && body.type !== 'service')
            ) {
                throw new Error('Bad request');
            }

            let asset;

            if (body.type === 'product') {
                asset = await this.productRepository.findOneOrFail(body.id);
            } else {
                asset = await this.serviceRepository.findOneOrFail(body.id);
            }

            // create a sale

            const sale = new Sale();

            sale.type = body.type;
            sale.userId = user.id;
            sale.assetId = asset.id;
            sale.price = asset.price;
            sale.qty = body.qty;
            sale.createdAt = new Date();

            if (body.type === 'product') {
                asset.qty = asset.qty - sale.qty;
                await this.connection.manager.save(asset);
            }

            // make client devoted

            const supplierUser = await this.setClientAsDevoted(user.id, asset.supplierId);

            await this.connection.manager.save([sale, supplierUser]);

            result.success = true;
        } catch (e) {
            result.message = e.message;
        }

        return result;
    }

    private generateToken(length) {
        let result = '';

        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        const charactersLength = characters.length;

        for (let i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }

        return result;
    }

    private async tryAuthenticate(token) {
        if (token.length === User.authTokenLength) {
            let user = await this.userRepository.findOne({
                token
            });

            if (user) {
                if (moment(user.tokenExpireAt).diff(moment()) < 0) {
                    throw new Error('Token expired.');
                } else {
                    return user;
                }
            } else { // try to log in user
                throw new Error('Unable to authenticate.');
            }
        } else if (token.length === Supplier.authTokenLength) {
            let supplier = await this.supplierRepository.findOne({
                token
            });

            if (supplier) {
                if (moment(supplier.tokenExpireAt).diff(moment()) < 0) {
                    throw new Error('Token expired.');
                } else {
                    return supplier;
                }
            } else { // try to log in supplier
                throw new Error('Unable to authenticate.');
            }
        } else {
            throw new Error('Unable to authenticate.');
        }
    }

    private async setClientAsDevoted(userId, supplierId) {
        let supplierUser = await this.supplierUserRepository.findOne({
            supplierId,
            userId
        });

        if (!supplierUser) {
            supplierUser = new SupplierUser();

            supplierUser.userId = userId;
            supplierUser.supplierId = supplierId;
            supplierUser.isDevoted = true;
        }

        return supplierUser;
    }
}
