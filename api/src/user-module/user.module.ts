import {Module} from "@nestjs/common";
import {UserController} from "./user.controller";
import {TypeOrmModule} from "@nestjs/typeorm";
import {User} from "../entity/User";
import {Supplier} from "../entity/Supplier";
import {Product} from "../entity/Product";
import {Offer} from "../entity/Offer";
import {Service} from "../entity/Service";
import {Sale} from "../entity/Sale";
import {SupplierUser} from "../entity/SupplierUser";

@Module({
    imports: [
        TypeOrmModule.forFeature([User, Supplier, Product, Service, Offer, Sale, SupplierUser])
    ],
    controllers: [
        UserController
    ]
})
export class UserModule {
}
