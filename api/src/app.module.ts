import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {UserModule} from "./user-module/user.module";
import {TypeOrmModule} from "@nestjs/typeorm";
import {SupplierModule} from "./supplier-module/supplier.module";

@Module({
    imports: [
        TypeOrmModule.forRoot(),
        UserModule,
        SupplierModule
    ],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule {
}
